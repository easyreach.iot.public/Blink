/*
*	This is the main file containing the core logic to blink an LED on GPIO pin PA3.
*	The code uses timer 0 at the frequency of 4 to do so.
*/

#include"stdio.h"
#include"Nano100Series.h"


unsigned char status=1;

/*
*	Timer 0 interrupt handler:
*/
void TMR0_IRQHandler(void)
{
	status=!status;
	PA3=status;
	TIMER_ClearIntFlag(TIMER0);
}

/*
*	This function will initialize the system clocks and GPIO multiplexed functions so that system can comunicate over UART0.
*/
void init(void)
{
	SYS_UnlockReg();
	
	CLK_SetHCLK(CLK_CLKSEL0_HCLK_S_HIRC,CLK_HCLK_CLK_DIVIDER(1));
	
	CLK_EnableXtalRC(CLK_PWRCTL_HIRC_EN);
	CLK_WaitClockReady(CLK_CLKSTATUS_HIRC_STB_Msk);
	
	CLK_SetModuleClock(UART0_MODULE,CLK_CLKSEL1_UART_S_HIRC,CLK_UART_CLK_DIVIDER(1));
	CLK_SetModuleClock(TMR0_MODULE,CLK_CLKSEL1_TMR0_S_HIRC,0);
	
	CLK_EnableModuleClock(UART0_MODULE);
	CLK_EnableModuleClock(TMR0_MODULE);
	
	SYS->PB_L_MFP &= ~(SYS_PB_L_MFP_PB0_MFP_Msk | SYS_PB_L_MFP_PB1_MFP_Msk);
	SYS->PB_L_MFP |= (SYS_PB_L_MFP_PB1_MFP_UART0_TX | SYS_PB_L_MFP_PB0_MFP_UART0_RX);
		
	SYS_LockReg();
}

int main()
{
	init();
	
	GPIO_SetMode(PA,BIT3,GPIO_PMD_OUTPUT);
	
	PA3=0;
	
	PA3=1;

	UART_Open(UART0,115200);
	
	printf("LED blink sample program\n");
	
	TIMER_Open(TIMER0,TIMER_PERIODIC_MODE,4);
	TIMER_EnableInt(TIMER0);
	NVIC_EnableIRQ(TMR0_IRQn);
	
	while(!TIMER_IS_ACTIVE(TIMER0))
		TIMER_Start(TIMER0);
	
	while(1)
	{
		__nop();
	}
}
